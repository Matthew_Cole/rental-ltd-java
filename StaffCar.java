package carCompany;

@SuppressWarnings("serial")
public class StaffCar extends Car {
	
	String Borrower;
	boolean Availability;

public StaffCar() {
	
	super();
	Borrower ="AVAILABLE";
	Availability=true;
}

public StaffCar(String aRegNo, String aModel, String aBorrower, int aMileage) throws CarException {
	
	super(aRegNo, aModel, aMileage);
	SetBorrower(aBorrower);
}

public void SetBorrower(String aBorrower) throws CarException {
	
	if(aBorrower.length()==0)throw new CarException("Borrower name must be enterd!");
	Borrower=aBorrower.toUpperCase().trim();
}

public String GetBorrower() {
	return Borrower;
}

public void print(String header) {
	
	super.Print(header);
	System.out.println("Borrower is: " +Borrower);	
}

public void Ask(String prompt) throws CarException {
	
	super.AskRegNo(prompt);
	super.AskModel(prompt);
	super.AskMileage(prompt);
	SetBorrower(Console.askString("Enter name of borrower: "));
}

public void AskBorrower(String prompt) throws CarException
{
	SetBorrower(Console.askString("Enter Borrower's name:"));
	Availability=false;
}

public String toString() { // Converts Integer to String.
	
	return super.toString() +"\t" +Borrower;
}


public void Print(String header)
{
	super.Print(header);
	System.out.println("Borrower: " +Borrower);
	System.out.println();
}
	
}
