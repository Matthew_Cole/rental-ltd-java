package carCompany;

public class Testing {
	
	public static void main(String[] args) {
		
		//Testing Default Constructor.
		Car car1 = new Car();
		car1.Print("Details for Car1 are: ");
		
		//Testing Parameterised Constructor.
		Car car2 = new Car("AAA111", "Toyota", 500);
		car2.Print("Details for Car2 are: ");
		
		//Ask user for Input.
		Car car3 = new Car();
		car3.Ask("Enter details for Cars: ");
		car3.Print("Details for Car3 are: ");
		
		//Use of toString Method.
		System.out.println("Data for Car 1: " + car1);
		System.out.println("Data for Car 2: " + car2);
		System.out.println("Data for Car 3: " + car3);

	}

}
