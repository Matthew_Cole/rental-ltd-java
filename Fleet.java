package carCompany;

import java.util.ArrayList; // Imports the ArrayList.
import java.io.*;

public class Fleet {
	
	ArrayList<Car>Fleet; //Creates the ArryList Fleet.
	ArrayList<String>Keys; //Creates the ArrayList Keys (Find Method).

public Fleet() {
	
	Fleet = new ArrayList<Car>(); //This is a new container to hold the instances of the Car class.
	Keys = new ArrayList<String>(); //This container is used to help us locate information that has been entered.
	
}

public int GetSize() { //Method used to return the size of the ArrayList.
	
	return Fleet.size(); //Returns the total number of Cars in the ArrayList.
	
}

public void Add(Car aCar) {
	
	Fleet.add(aCar); //Adds Car (aCar) to the ArrayList.
	Keys.add(aCar.GetRegNo()); //Adds Car and is stored with RegNo.
	
}

public Car Find(String aRegNo) { //Find() method.
	
	int index = Keys.indexOf(aRegNo);
	if(index == -1) //Car Not Found.
		return null;
	else
		return Fleet.get(index);
	
}
	
public void Print(String header) {
	
	System.out.println(header);
	for (int i=0; i< Fleet.size(); i++) //For each car in the ArrayList.
		System.out.println( Fleet.get(i)); //Get it and print it.
	
}

public void Save(String fileName) throws CarException {
	
	FileOutputStream outputFile;
	
	try {
		
		outputFile = new FileOutputStream(fileName);
		
	}
	catch (IOException io) {
		
		throw new CarException("Cannot create" +fileName);
		
	}
	ObjectOutputStream fleetFile; // dataFile is fleetFile.
	try { // Writes object to dataFile.
		
		fleetFile = new ObjectOutputStream(outputFile);
		fleetFile.writeObject(Keys); // Keys will hold regNos.
		fleetFile.writeObject(Fleet);
		fleetFile.close();
		
	}
	catch (FileNotFoundException e) {
		
		throw new CarException("Cannot create" +fileName);
		
	}
	catch (IOException io) {
		
		throw new CarException("Cannot write" +fileName);
		
	}
	
}

public void Open(String fileName) throws CarException {
	
	FileInputStream inputFile;
	
	try { // To open file.
		
		inputFile = new FileInputStream(fileName);
		
	}
	catch (FileNotFoundException e) {
		
		throw new CarException("Cannot open" +fileName);
		
	}
	ObjectInputStream fleetFile;
	try { // To read file.
		
		fleetFile = new ObjectInputStream(inputFile);
		Keys=(ArrayList<String>)fleetFile.readObject();
		Fleet=(ArrayList<Car>)fleetFile.readObject();
		
	}
	catch (IOException e) {
		
		throw new CarException("Error reading from" +fileName);
		
	}
	catch (ClassNotFoundException e) {
		
		throw new CarException("Error reading from" +fileName);
		
	}
	try { //To close file.
		
		fleetFile.close();
		
	}
	catch (IOException e) {
		
		throw new CarException("Cannot close" +fileName);
		
	}
	
}

}	
