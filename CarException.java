package carCompany;

public class CarException extends Exception { // Extends from the java exception.
	
	private static final long serialVersionUID = 1L;

	public CarException (String aMessage) {
		
		super (aMessage);
		
	}
	
}
