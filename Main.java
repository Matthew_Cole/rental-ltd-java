package carCompany;
// Menu driven application for Cars.

public class Main {
	
	static Fleet fleet = new Fleet();
	static StaffCar staffCar;
	static String regNo;
	static boolean finished = false; //Not Finished.
	static char option;
	String fileName;

	public static void main(String[] args) throws CarException {
		
		System.out.println("~ WELCOME TO THE CAR LIBRARY ~\n");

		start();
		while(!finished) { //Not finished.
			try {
			option = Console.askOption("Select A)dd, F)ind, B)orrow, R)eturn, P)rint, Q)uit");
			switch (option) {
			
			case 'A': addHandler();  break;
			case 'F': findHandler(); break;
			case 'B': borrowHandler(); break;
			case 'R': returnHandler(); break;
			case 'P': printHandler(); break;
			case 'Q': quitHandler(); break;
			default: System.out.println("Error: Invalid Option");
			} //end switch.
			}
			
			catch (CarException i) {
				
				System.out.println("Error:" +i.getMessage());
				
			}
			
		} // end while.
		
	} // end main method.
			
public static void addHandler() throws CarException {
	
	staffCar = new StaffCar();
	staffCar.AskRegNo("Enter Registration Number: ");
	if (fleet.Find(staffCar.GetRegNo()) != null ) { // Check for duplicates.
		
		System.out.println("ERROR: Registration Number already exists, Car not Added!\n");
		
	}
	
	else {
		
		staffCar.AskModel("Enter Model:");
		staffCar.AskMileage("Enter Mileage:");
		fleet.Add(staffCar);
		
	}
	
}

public static void printHandler() {
	
	fleet.Print("Cars are: ");
	
}
	
public static void findHandler() throws CarException {
	
	staffCar=new StaffCar();
	staffCar.AskRegNo("Enter car reg number; ");
	if ((StaffCar)fleet.Find(staffCar.GetRegNo())==null) {
		
		System.out.println("Car doesnt exist");
		
	}
	else {
		
		System.out.println("Car found is: " +(StaffCar)fleet.Find(staffCar.GetRegNo()));
		
	}
	
}

public static void borrowHandler() throws CarException {
	
	staffCar=new StaffCar();
	staffCar.AskRegNo("Enter Registration Number: ");
	staffCar = (StaffCar)fleet.Find(staffCar.GetRegNo());
	if (staffCar==null) {
		
		System.out.println("Car does not exist");
		
	}
	else if(staffCar.Availability == false) {
		
		System.out.println("Car is on loan");
		
	}
	else {
		
		staffCar.AskBorrower("Enter borrowers name: ");
		
	}
	
}
			
public static void returnHandler()throws CarException {
	
	staffCar= new StaffCar();
	staffCar.AskRegNo("Enter Registration Number: ");
	staffCar=(StaffCar)fleet.Find(staffCar.GetRegNo());
	if((staffCar != null) && (staffCar.GetBorrower()!="AVAILABLE")) {
		
		regNo="AVAILABLE";
		staffCar.Availability=true;
		System.out.println("Mileage before hire: " +staffCar.GetMileage());
		staffCar.AskMileage("Enter new Mileage ");
		System.out.println("Updating...");
		System.out.println("Car with Registration Number: " +staffCar.GetRegNo()+" & Mileage of " +staffCar.GetMileage()+" Returned!");
		staffCar.SetBorrower(regNo);
		
	}
	else {
		
		System.out.println("Error: This car is not registerd with us!");
		
	}
	
}
	
public static void quitHandler() throws CarException {
	
	try {
		
		fleet.Save("CarFile.uwl");
		
	}
	catch (CarException e) {
		
		System.out.println("File not found");
		
	}
	
	finished=true; // Finished.
	System.out.println("End of Application");
	
}

public static void start() {
	
	try {
		
		fleet.Open("CarFile.uwl");
		
	}
	catch(CarException e) {
		
		System.out.println("Notice: File not yet created");
		
	}
	
}

}
