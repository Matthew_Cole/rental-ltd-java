package carCompany;

import java.io.*;

@SuppressWarnings("serial")
public class Car implements Serializable {
	
	//Properties.
	String regNo;
	String model;
	int mileage;
	
	//Default Constructors.
	public Car() {
		
		Set ("??????", "Unknown", 0);
		
	}
	
	//Constructors With Parameters.
	public Car (String aRegNo, String aModel, int aMileage) {
		
		Set (aRegNo,aModel,aMileage);
		
	}
	
	//Set Method for Whole Object.
	public void Set (String aRegNo, String aModel, int aMileage) {
		
		regNo = aRegNo;
		model = aModel;
		mileage = aMileage;
		
	}
	
	//Individual Set Methods.
	public void SetRegNo (String aRegNo) throws CarException {
		
		if(aRegNo.length()==0)throw new CarException("RegNo must be given");
		regNo = aRegNo.trim();
		
	}
	
	public void SetModel (String aModel) throws CarException {
		
		if(aModel.length()==0)throw new CarException("Please type car model");
		model = aModel.trim();
		
	}
	
	public void SetMileage (int aMileage) throws CarException {
		
		if(aMileage<0) throw new CarException("Mileage must be positive");
		mileage = aMileage;
		
	}
	
	//Ask Method for Whole Object.
	public void Ask (String Prompt) throws CarException {
		
		try {
		
		SetRegNo (Console.askString ("Enter Registration Number: "));
		SetModel (Console.askString ("Enter Model: "));
		SetMileage (Console.askInt ("Enter Mileage: "));
		System.out.println();
		
		}
		catch(NumberFormatException e) {
			
			throw new CarException ("Mileage must be an Integer");
			
		}
		
	}
	
	//Individual Ask Methods.
	public String AskRegNo (String prompt) throws CarException {
		
		SetRegNo(Console.askString("Enter Registration Number: "));
		return regNo;
		
	}
	
    public String AskModel (String prompt) throws CarException {
    	
    	SetModel(Console.askString("Enter Model: "));
		return model;
		
    }
    
    public int AskMileage (String prompt) throws CarException {
    	
    	try {
    		
    		SetMileage(Console.askInt("Enter Mileage: "));
    		return mileage;
    		
    	}
    	catch(NumberFormatException e) {
    		
    		throw new CarException ("Mileage must be an Integer");
    		
    	}
    	
    }
	
	//Get Method for Whole Object.
	public void Get (String aRegNo, String aModel, int aMileage) {
		
		regNo = aRegNo;
		model = aModel;
		mileage = aMileage;
		
	}
	
	//Individual Get Methods.
	public String GetRegNo () {
		
		return regNo;
		
	}
	
	public String GetModel () {
		
		return model;
		
	}
	
	public int GetMileage () {
		
		return mileage;
		
	}
	
	//Print Method.
	public void Print() {
		
		System.out.println("Registration Number is:" + regNo);
		System.out.println("Model is:" + model);
		System.out.println("Mileage is:" + mileage);
		
	}
	
	//Overloading Print Method.
	public void Print(String header) {
		
		System.out.println(header);
		Print();
		
	}
	
	//ToString() Method.
	public String toString() {
		
		return regNo + " " + model + " " + mileage;
		
	}
	
}
